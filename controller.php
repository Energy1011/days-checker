<?php 
include('src/bootstrap.php'); 

function stats_for_graph_get(){
  $db = new DB;
  $query = "SELECT * FROM cat_topics;";
  $result = $db->query($query);
  $rows = array();

  // Check the number of columns
  if(mysqli_num_rows($result) >= 1){
    // Here begins query to get stats
    $query = "SELECT";
    for($e = 1; $e <= mysqli_num_rows($result); $e++){
      $query .= " sum(c".$e.") as c".$e.",";
    }
    $query = substr($query, 0, -1);// remove last comma
    $query .= " FROM days;";
    // Query to get stats
    $result = $db->query($query);
    // Are there stats ?
    if(mysqli_num_rows($result) >= 1){
      while($r = mysqli_fetch_assoc($result)) {
        $rows[] = $r;
      }
      $code = 200;
    }else{
      $code = 404;
    }
  }else{
    $code = 404;
  }
  // Response
  echo json_encode(array('code' => $code, 'data' => $rows));
}

function cat_topics_get(){
  // Get all topics from catalog
  $db = new DB;
  $query = "SELECT * FROM cat_topics;";
  $result = $db->query($query);
  $rows = array();

  while($r = mysqli_fetch_assoc($result)) {
    $rows[] = $r;
  }
  if(mysqli_num_rows($result) >= 1){
    $code = 200;
  }else{
    $code = 404;
  }
  // Response
  echo json_encode(array('code' => $code, 'data' => $rows));
}

/**
 * Insert day information 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2017 
 * @param   checked_responses 
 * @return  JSON 
 */ 
function insert_day_info_post(){
  global $app;
  $msg = "";
  $responses = explode(',',$_POST['responses']);
  $day_comment = $_POST['day_comment'];
  $passwd = $_POST['passwd'];
  //Check for password to insert into db
  if ($passwd == $app->config['passwd']){
    $query = "INSERT INTO `".$app->config['dbname']."`.`days` (`id`, ";
    $columns = "";
    for($e=1; $e <= count($responses); $e++){
      $columns .= "`c".$e."`, ";
    }	

    $query .= $columns."`day_comment`,`creation`) VALUES ( NULL,";

    // Iter over each response to get value	
    foreach($responses as $value){
      $query .= "'".$value."',";
    }
    // Day comment
    $query .="'".$day_comment."',";
    $query .= "CURRENT_TIMESTAMP);";

    $db = new DB;
    if($db->query($query)){
      $msg = "Day saved OK :)";
      $code = 200;
    }else{
      $msg = "DB error";
      $code = 400;
    }
  }else{
    $msg = "Wrong password or service not available";
    $code = 400;
  }
  // Allow CORS

  // Response
  echo json_encode(array('code' => $code, 'msg'=>$msg));

}

/**
 * Dispatch request http and call the function
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2016
 */ 
function dispatch(){
  //TODO: clear input
  // Add headers and CORS
  header("Access-Control-Allow-Origin: *");
  header('Access-Control-Allow-Methods: GET, POST');
  header('Access-Control-Allow-Headers: Content-Type');
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $call = $_POST['func']."_post";	
  }

  if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $call = $_GET['func']."_get";	
  }

  if(function_exists($call)){
    $call();
  }
}

// Dispatch request
dispatch();
?>
