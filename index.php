<?php 
// Includes
include('src/bootstrap.php');
//Session
session_start();
//(Energy1011) TODO: add message of the day
//(Energy1011) TODO: add day free text in text area 
?>
<html>
<head>
        <meta charset="UTF-8">
        <title><?php lang('day_checker'); ?></title>
        <!-- Bootstrap css--!>
        <link rel="stylesheet" href="js/bootstrap/css/bootstrap.min.css"> 
        <script src="js/jquery/jquery.min.js"></script>
        <script src="js/bootstrap/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js"></script>
        <script src="js/app.js"></script>
<style>
body {
        margin-left: 5px;
}
</style>
</head>
<body>
        <div>
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                        <div class="navbar-header">
                                <span class="navbar-text navbar-left"><?php lang('day_checker'); ?></span>
                        </div>
                  </div>
                </nav>
        </div>
        <div>
                <h3><span class="label label-default"><?php lang('list'); ?></span> <span id="div-curr-directory"></span></h3> 
        </div>
        <div class="panel panel-default">
                <div id="div_list">
                </div>
                <textarea name="day_comment" id="day_comment" rows="8" cols="20" placeholder="<?php lang('write_day_comment'); ?>"></textarea>
                <div>
                        <input type="password" id="passwd" placeholder="Password"/>
                        <button onclick="insert_day();" class="btn btn-primary"><?php lang("save"); ?></button>
                </div>
        <canvas id="myChart" width="400" height="400"></canvas>
        </div>
</body>
</html>
