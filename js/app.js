// appData global JSON general data
var appData= { 
	"curr_page_num": 0,
	"parent_dir": "",
	"curr_dir": "",
	"total_items":0,
	"page_size":0,
	labels: []
};
/**
 * Insert day in db
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2017
 */ 
function insert_day(){
	var checked_responses = [];
	if( $("#passwd").val() == ''){
		alert("Enter a password please");
		return;
	}
	// Iter over checkboxes
	$("input:checkbox").each(function (){
		checked_responses.push(this.checked ? "1" : "0");
	});	
	// Get general day comment
	var day_comment = '';
	if ($("#day_comment").val() != 'undefined' || $("#day_comment").val() != ''){
		day_comment = $("#day_comment").val(); 
		console.log(day_comment);
	}
	$.ajax({
		url: "controller.php",
		dataType: "json",
		method: "POST",
		data: {  func:"insert_day_info",
			responses:checked_responses,
			day_comment:day_comment,
			passwd:$("#passwd").val()
		}
	}).done(function(response) {
		if(response.code == 200){
			alert("Ok.");
		}else{
		 	alert("There is an error.");
		}
	});
}

/**
 * Get topics list from db 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2017
 */ 
function get_topics(){
	$.ajax({
		url: "controller.php",
		dataType: "json",
		method: "GET",
		data: {  func:"cat_topics"}
	}).done(function(response) {
		if(response.code == 200){
			$("#div_list").html('');
			for (var item in response.data){
				appData.labels.push(response.data[item].topic_name);
				$("#div_list").append("<input type='checkbox'> <label>"+response.data[item].topic_name+"</label><br>");
			}
		}else{
			console.log("Error get topics");
		}
	});
}

function draw_chart(response_data){
	response_data = response_data[0];
	var total_cols= [];
	var labels = [];
	for(let col in response_data){
		labels.push(col);
		total_cols.push(parseInt(response_data[col]));
	}
	var ctx = document.getElementById("myChart").getContext("2d");
	var myChart = new Chart(ctx, {
		    type: 'bar',
		data: {
			labels: appData.labels,
			datasets: [{
				label: 'Stats',
				data: total_cols,
				borderWidth: 1
			}]
			    
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
							beginAtZero:true
					}
				}]
			}
		}

	});
}

/**
 * Get stats data for the graph 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2017
 * @return  JSON
 */ 
function get_stats(){
	$.ajax({
		url: "controller.php",
		dataType: "json",
		method: "GET",
		data: {  func:"stats_for_graph" }
	}).done(function(response) {
		if(response.code == 200){
			draw_chart(response.data);
		}else{
			console.log("Error get topics");
		}
	});
}


// Get topics for the list
get_topics();
// Get stats for the graph
get_stats();
