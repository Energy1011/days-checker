<?php

//load config file
$app = New MyAPP;
$app->load_config();

//Load lang functions
include('src/lang.php');
include('src/db.php');


/**
 * Main class to bootstrap the app object
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2017
 */ 
class MyAPP{
  public $config = Array();

  /**
   * Load config file in app->config global-object
   * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2017
   */ 
  function load_config(){
    require_once('config.php');
    $this->config = get_config();
  }

}
?>
