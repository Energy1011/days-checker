
-- This table is where you need to put each element in your list
CREATE TABLE IF NOT EXISTS `cat_topics` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `topic_name` varchar(128) NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

	-- --------------------------------------------------------

-- This table is where results are saved
-- Here are c15 columms thats why I have 15 topic in cat_topics
--

CREATE TABLE IF NOT EXISTS `days` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `c1` tinyint(4) NOT NULL,
	  `c2` tinyint(4) NOT NULL,
	  `c3` tinyint(4) NOT NULL,
	  `c4` tinyint(4) NOT NULL,
	  `c5` tinyint(4) NOT NULL,
	  `c6` tinyint(4) NOT NULL,
	  `c7` tinyint(4) NOT NULL,
	  `c8` tinyint(4) NOT NULL,
	  `c9` tinyint(4) NOT NULL,
	  `c10` tinyint(4) NOT NULL,
	  `c11` tinyint(4) NOT NULL,
	  `c12` tinyint(4) NOT NULL,
	  `c13` tinyint(4) NOT NULL,
	  `c14` tinyint(4) NOT NULL,
	  `c15` tinyint(4) NOT NULL,
	  `day_comment` varchar(512) DEFAULT NULL,
	  `creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	  PRIMARY KEY (`id`)
	) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;
