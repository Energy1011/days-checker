<?php 
/**
 * DB connection class 
 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2017
 */ 
class DB{
	/**
	 * Open a connection to db 
	 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2017
	 * @return  Object mysql connection 
	 */ 
	function open_connection(){
		global $app;
                /*
                echo "dbserver:".$app->config['dbserver']."<br>";
                echo "dbusernam:".$app->config['dbusername']."<br>";
                echo "pass".$app->config['dbpassword']."<br>";
                echo "dbname".$app->config['dbname']."<br>";
                die();
                 */
		$con = mysqli_connect($app->config['dbserver'], $app->config['dbusername'], $app->config['dbpassword'], $app->config['dbname']);
		if (!mysqli_set_charset($con, "utf8")) {
			printf("Error loading character set utf8: %s\n", mysqli_error($con));
			exit();
		}
		mysqli_select_db($con, $app->config['dbname']);
		if(!$con){
			die('Could not connect:'.mysqli_error($con));
		}else{
			return $con;
		}
	}

	/**
	 * Close a connection 
	 * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2017
	 * @param   Object mysql connection to close 
	 * @return  Boolean 
	 */ 
	function close_connection($con){	
		return mysqli_close($con);
	}

	/**
	 * Execute a query * @author  4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com) 2017
	 * @param   String $query
	 * @return  Mysql result 
	 */ 
	function query($query){
		global $app;
		$con = $this->open_connection();
		$result = mysqli_query($con, $query);
		if (!$result) {
			    die('Invalid query: '.mysqli_error($con));
		}
		$this->close_connection($con);
		return $result;
	}

}
?>
